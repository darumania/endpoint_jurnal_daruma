FROM ubuntu:18.04

MAINTAINER Farhan "farhan@daruma.co.id"

RUN apt-get update -y && apt-get install -y -q python3 python3-pip

COPY ./requirements.txt /app/api/requirements.txt

WORKDIR /app/api

RUN pip3 install -r requirements.txt

COPY . /app

CMD [ "python3", "manage.py", "runserver",  "0.0.0.0:8181" ]

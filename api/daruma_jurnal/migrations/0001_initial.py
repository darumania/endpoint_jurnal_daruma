# Generated by Django 2.2 on 2019-04-12 07:08

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Jurnal_Jobs',
            fields=[
                ('job_id', models.AutoField(primary_key=True, serialize=False)),
                ('data_input', models.TextField()),
                ('status', models.CharField(default='pending', max_length=255)),
                ('response', models.TextField(default='-')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
            ],
        ),
    ]

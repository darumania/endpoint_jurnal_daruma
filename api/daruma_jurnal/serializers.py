from rest_framework import serializers
from .models import Jurnal_Jobs, Jurnal_Jobs_Live


class Jurnal_JobsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Jurnal_Jobs
        fields = ("job_id", "data_input", "status")


class Jurnal_Jobs_liveSerializer(serializers.ModelSerializer):

    class Meta:
        model = Jurnal_Jobs_Live
        fields = ("job_id", "data_input", "status")
from django.shortcuts import render
from rest_framework import generics
from .models import Jurnal_Jobs, Jurnal_Jobs_Live
from .serializers import Jurnal_JobsSerializer, Jurnal_Jobs_liveSerializer
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.parsers import FormParser, JSONParser, MultiPartParser
import json
from django.template import loader
import logging
import pprint
import ast

logging.basicConfig(level=logging.DEBUG)

# Create your views here.
class ListJurnal_JobsView(APIView):

    parser_classes = (MultiPartParser, FormParser, JSONParser,)

    def get(self, request, version, format=None):
        job = Jurnal_Jobs.objects.all()
        serializer = Jurnal_JobsSerializer(job, many=True)
        return Response(serializer.data)

    def post(self, request, version, format=None):
        logging.info(f'listjobview: {request.data}')
        try:
            print(f'listjobview: {request.data}')
        except:
            pass


        serializer = Jurnal_JobsSerializer(data={'data_input':str(request.data)})

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'POST'])
def jurnal_job(request, version):
    if request.method == 'GET':
        job = Jurnal_Jobs.objects.all()
        serializer = Jurnal_JobsSerializer(job, many=True)
        return Response(serializer.data)
    elif request.method == 'POST':
        data_input = dict(request.POST.lists())['data_input']
        data = dict(request.POST.lists())
        logging.info(f'data list: {request.data}')
        data['data_input'] = str(data_input)
        post = Jurnal_Jobs()
        post.data_input = data['data_input']
        logging.info(f"data_input: {data['data_input']}")
        post.save()
        return Response(status=status.HTTP_201_CREATED)


def view_response(request, version):
    job = Jurnal_Jobs.objects.all().order_by('-job_id')
    res = []
    for j in job:
        res.append(ast.literal_eval(j.response))
    result = zip(job, res)
    context = {
        "jobs" : result
    }
    return render(request, 'daruma_jurnal/view_response.html',context=context)

class ListJurnal_JobsView_Live(APIView):

    parser_classes = (MultiPartParser, FormParser, JSONParser,)

    def get(self, request, version, format=None):
        job = Jurnal_Jobs_Live.objects.all()
        serializer = Jurnal_Jobs_liveSerializer(job, many=True)
        return Response(serializer.data)

    def post(self, request, version, format=None):
        logging.info(f'listjobview: {request.data}')
        try:
            print(f'listjobview: {request.data}')
        except:
            pass


        serializer = Jurnal_JobsSerializer(data={'data_input':str(request.data)})

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


def view_response_live(request, version):
    job = Jurnal_Jobs_Live.objects.all().order_by('-job_id')
    res = []
    for j in job:
        res.append(ast.literal_eval(j.response))
    result = zip(job, res)
    context = {
        "jobs" : result
    }
    return render(request, 'daruma_jurnal/view_response.html',context=context)
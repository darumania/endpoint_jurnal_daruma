from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APITestCase, APIClient
from rest_framework.views import status
from .models import Jurnal_Jobs
from .serializers import Jurnal_JobsSerializer

# Create your tests here.
class BaseViewTest(APITestCase):

    client = APIClient()

    @staticmethod
    def insert_job(data_input=None):
        if data_input is not None:
            Jurnal_Jobs.objects.create(data_input=data_input)


    def setUp(self):
        # add data test
        self.insert_job(data_input="testing insert data")

class GetAllJurnal_JobsTest(BaseViewTest):

    def test_get_all_Jurnal_Jobs(self):

        response = self.client.get(
            reverse("Jurnal_Jobs-all", kwargs={"version": "v1"})
        )

        expected = Jurnal_Jobs.objects.all()
        serialized = Jurnal_JobsSerializer(expected, many=True)
        self.assertEqual(response.data, serialized.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)